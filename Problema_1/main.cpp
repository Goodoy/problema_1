/*Ciclo que vaya restando la n-posicion del arreglo de clases de monedas y contabilizando el numero de veces que
realiza esta operacion y luego imprima cuantas de unidades de cada clase se necesitan para retornar todo el monto
El retorno debe ser:si se ingresa 47810, el programa debe imprimir:
50000 : 0
20000: 2
10000 : 0
5000: 1
2000 : 1
1000: 0
500 : 1
200: 1
100 : 1
50: 0
Faltante: 10.*/

#include <iostream>

using namespace std;

void imprimirCambio(int *uni); //Funcion que recibe un arreglo y lo imprime posicion por posicion, reacionado con
                               //el arreglo numero.

int numero[]={50000,20000,10000,5000,2000,1000,500,200,100,50}; //Arreglo que tiene todas las clases de monedas:


int main()
{
    int unidades[]={0,0,0,0,0,0,0,0,0,0,0}; // Inicializacion del arreglo para guardar las unidades.
    int cambio,i=0; //Inicializacion de variables

    cout<<"Cual es el cambio: "; cin>>cambio;  //Ingreso del monto

    while(cambio>=50){ //Condicional que termina cuando ya no sea posible restarla minima clase.
        if(cambio>=numero[i]){ //Si es mayor a la case ubicada en i
            cambio-=numero[i]; //Se le resta la clase i
            unidades[i]+=1; //Se guarda la unidad en el arreglo de unidades
        }else{
            i++; //Se mueve una posicion solo cuando no es posible restarle
        }

    }

    imprimirCambio(unidades); //Imprimir el cambio, recorriendo el arreglo unidades
    cout<<"Restante : "<<cambio<<endl; //Imprime el restante

    return 0;
}

void imprimirCambio(int *uni)
{
    for(int i=0;i<11;i++){ //Recorre el arreglo
        cout<<numero[i]<<" : "<<*(uni+i)<<endl; //Imprime el formato deseado

    }

}
